const express = require('express');
const { google } = require('googleapis');
const { OAuth2Client } = require('google-auth-library');

const app = express();
const PORT = process.env.PORT || 3000;

// OAuth 2.0 credentials
const CLIENT_ID = '623328983369-dt4fv2nsdvohqbtlrdcn6i728n0gb4fs.apps.googleusercontent.com';
const CLIENT_SECRET = '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';
const REDIRECT_URI = 'http://localhost:3000/auth/google/callback';
const SCOPES = [
  "https://www.googleapis.com/auth/userinfo.profile",
  "https://www.googleapis.com/auth/userinfo.email"
];

// Create OAuth 2.0 client
const oauth2Client = new google.auth.OAuth2(
  CLIENT_ID,
  CLIENT_SECRET,
  REDIRECT_URI
);

// Route to initiate the OAuth 2.0 authentication process
app.get('/auth/google', (req, res) => {
  const authUrl = oauth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES
  });
  res.redirect(authUrl);
});

// Route to handle the callback from Google after authentication
app.get('/auth/google/callback', async (req, res) => {
  const code = req.query.code;

  try {
    const { tokens } = await oauth2Client.getToken(code);
    oauth2Client.setCredentials(tokens);

    res.status(200).json({ success: true, tokens });
  } catch (error) {
    res.status(500).json({ success: false, message: 'Authentication failed!' });
  }
});

/***
 * Verify token
 */
const client = new OAuth2Client(CLIENT_ID);

app.get('/verify', async (req, res) => {
  const { token } = req.query;

  try {
    const ticket = await client.verifyIdToken({
      idToken: token,
      audience: CLIENT_ID,
    });

    const payload = ticket.getPayload();

    res.status(200).json({ success: true, payload });
  } catch (error) {
    res.status(401).json({ success: false, message: 'Invalid token', error });
  }
});

// Start the server
app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
